FROM ubuntu:16.04

ENV DEBIAN_FRONTEND noninteractive

# Need wine 1.7.xx for this all to work, so we'll use the PPA:
RUN dpkg --add-architecture i386 \
 && apt-get update \
 && apt-get install --no-install-recommends -qfy wget software-properties-common apt-transport-https \
 && wget -qO - https://dl.winehq.org/wine-builds/Release.key | apt-key add - \
 && apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/' \
 && apt-get update \
 && apt-get install --no-install-recommends -qfy winehq-stable xvfb xauth git-core \
 && apt-get clean

RUN wget -nv https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks -O /usr/bin/winetricks \
 && chmod +x /usr/bin/winetricks 

RUN useradd -d /app -m app
USER app
ENV HOME /app
RUN mkdir -p /app/src
RUN mkdir -p /app/.profile.d
WORKDIR /app/src

ENV WINEARCH win32
# Silence all the "fixme: blah blah blah" messages from wine
ENV WINEDEBUG fixme-all

# Install the .net 4.0 runtime (not actually required for python!)
RUN wineboot \
 && winetricks -q dotnet40 \
 && while pgrep wineserver >/dev/null; do echo "Waiting for wineserver"; sleep 1; done \
 && rm -rf $HOME/.cache/winetricks

# Install Python.
# https://docs.python.org/3.5/using/windows.html#installing-without-ui
#RUN wget -nv https://www.python.org/ftp/python/3.6.1/python-3.6.1-amd64.exe \
# && xvfb-run -a wine python-3.6.1-amd64.exe /quiet \
#                                            TargetDir="C:\Python-3.6.1-amd64" \
#                                            InstallAllUsers=1 \
#                                            PrependPath=1 \
#                                            Shortcuts=0 \
#                                            Include_doc=0 \
#                                            Include_test=0 \
#                                            InstallLauncherAllUsers=1 \
# && rm python-3.6.1-amd64.exe \
ADD Python/Git/* /app/.wine/drive_c/windows/system32/
ADD Python/Python27 /app/.wine/drive_c/Python27
RUN echo 'wine python 2.7.13 at "C:\Python27\python.exe"' \
 && mkdir -p /app/bin \
 && echo 'wine '\''C:\Python27\python.exe'\'' "$@"' > /app/bin/python \
 && echo 'wine '\''C:\Python27\Scripts\pip.exe'\'' "$@"' > /app/bin/pip \
 && chmod +x /app/bin/* \
 && echo 'assoc .py=PythonScript' | wine cmd \
 && echo 'ftype PythonScript=c:\Python27\python.exe "%1" %*' | wine cmd \
 && while pgrep wineserver >/dev/null; do echo "Waiting for wineserver"; sleep 1; done \
 && rm -rf /tmp/.wine-*

ENV PATH ${HOME}/bin:${PATH}

ONBUILD COPY . /app/src
ONBUILD RUN test -e requirements.txt && pip install -r requirements.txt \
 && while pgrep wineserver >/dev/null; do echo "Waiting for wineserver"; sleep 1; done \
 && rm -rf /tmp/.wine-*
